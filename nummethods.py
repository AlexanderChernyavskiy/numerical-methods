import sys
from math import *

import matplotlib
import numpy as np
import pandas as pd
from PyQt4 import QtCore, QtGui, uic

matplotlib.use('Qt4Agg')

qtCreatorFile = "main.ui"  # мой Qt Designer file
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class PlotGraph(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.plot()

    def plot(self):
        try:
            dfs = pd.read_csv("s(t).csv", index_col='point')
            dfz = pd.read_csv("z(t).csv", index_col='point')
            f = self.canvas.figure.add_subplot(111)
            f.plot(dfs, label=u"S(t)")
            f.plot(dfz, label=u"z(t)")
            f.set_xlabel(u"t")
            f.grid()
            f.legend()
            self.canvas.draw()
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Предупреждение", u"Некорректный ввод функций S(t), z(t), либо y(t) не найдена!")

        try:
            dfy = pd.read_csv("y(t).csv", index_col='point')
            f = self.canvas_6.figure.add_subplot(111)
            f.plot(dfy, label=u"y(t)")
            f.set_xlabel(u"t")
            f.grid()
            f.legend()
            self.canvas.draw()
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Предупреждение", u"y(t) не найдена!")


        try:
            dfp = pd.read_csv("p(w).csv", index_col='point')
            t = self.canvas_2.figure.add_subplot(111)
            t.plot(dfp, label=u"p(w)")
            t.set_xlabel(u"w")
            t.grid()
            t.legend()
            self.canvas_2.draw()
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Предупреждение", u"Некорректный ввод функции p(w)!")

        try:
            dfsx = pd.read_csv("s(x).csv", index_col='point')
            t = self.canvas_4.figure.add_subplot(111)
            t.plot(dfsx, label=u"S(x)")
            t.set_xlabel(u"x")
            t.grid()
            t.legend()
            self.canvas_4.draw()
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Предупреждение", u"x(t) еще не найдена")

        try:
            dfx = pd.read_csv("x(t).csv", index_col='point')
            dfs = pd.read_csv("s(t).csv", index_col='point')
            dfsmx = pd.read_csv("s(t)-x(t).csv", index_col='point')
            t = self.canvas_3.figure.add_subplot(111)
            t.plot(dfx, label=u"x(t)")
            t.plot(dfs, label=u"S(t)")
            t.plot(dfsmx, label=u"S(t)-x(t)")
            t.set_xlabel(u"t")
            t.grid()
            t.legend()
            self.canvas_3.draw()
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Предупреждение", u"x(t) еще не найдена")

        try:
            c1b = pd.read_csv("c1(b).csv", index_col='point')
            c2b = pd.read_csv("c2(b).csv", index_col='point')
            f = pd.read_csv("f(b).csv", index_col='point')
            t = self.canvas_5.figure.add_subplot(111)
            t.plot(c1b, "-*", label=u"c1(b)")
            t.plot(c2b, "-*", label=u"c2(b)")
            t.plot(f, "-*", label=u"f(b)")
            t.set_xlabel(u"b")
            t.grid()
            t.legend()
            self.canvas_5.draw()
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Предупреждение", u"Значения критериев еще не найдены")



class AnyWidget(QtGui.QWidget):
    def __init__(self,*args):
        QtGui.QWidget.__init__(self, *args)
        self.setWindowTitle(self.trUtf8(u'Динамическая настройка параметров в рекламном сервере'))
        self.boxlay = QtGui.QHBoxLayout(self)

        self.frame = QtGui.QFrame(self)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)

        self.gridlay = QtGui.QGridLayout(self.frame)  # Менеджер размещения элементов во фрейме

        self.radio_group = QtGui.QGroupBox(u"Выберите режим работы", self.frame)  # Рамка с надписью вокруг группы элементов.
        self.radio_lay = QtGui.QVBoxLayout(self.radio_group)  # Менеджер размещения элементов в рамке.
        self.radio1 = QtGui.QRadioButton(u"Ручной", self.radio_group)  # Два зависимых переключателя
        self.radio2 = QtGui.QRadioButton(u"Автоматический", self.radio_group)
        self.radio1.setChecked(True)
        self.radio_lay.addWidget(self.radio1)
        self.radio_lay.addWidget(self.radio2)
        self.gridlay.addWidget(self.radio_group, 0, 0, 2, 1)

        self.label_p = QtGui.QLabel(u"Функция p(w) распределения аудитории по вероятности попадания в таргет:", self.frame)  # Текстовая метка.
        self.gridlay.addWidget(self.label_p, 3, 0)
        self.ln_edit_p = QtGui.QLineEdit(u"", self.frame)  # Строковое поле ввода.
        self.gridlay.addWidget(self.ln_edit_p, 3, 1)

        self.label_s = QtGui.QLabel(u"Функция S(t) показов по плану:", self.frame)
        self.gridlay.addWidget(self.label_s, 5, 0)
        self.ln_edit_s = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_s, 5, 1)

        self.label_z = QtGui.QLabel(u"Функция z(t) общего трафика:", self.frame)
        self.gridlay.addWidget(self.label_z, 7, 0)
        self.ln_edit_z = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_z, 7, 1)

        self.label_f = QtGui.QLabel(u"Функция f(x, z, S, b) коррекции параметра фильтрации по соц-дем признакам:", self.frame)
        self.gridlay.addWidget(self.label_f, 9, 0)
        self.ln_edit_f = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_f, 9, 1)

        self.label_b = QtGui.QLabel(u"Значение параметра b для ручного режима, интервал для автоматического*:", self.frame)
        self.gridlay.addWidget(self.label_b, 11, 0)
        self.ln_edit_b = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_b, 11, 1)

        self.label_x = QtGui.QLabel(u"Значение X_0 (для ручного режима):", self.frame)
        self.gridlay.addWidget(self.label_x, 13, 0)
        self.ln_edit_x = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_x, 13, 1)

        self.label_y = QtGui.QLabel(u"Значение Y_0:",self.frame)
        self.gridlay.addWidget(self.label_y, 15, 0)
        self.ln_edit_y = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_y, 15, 1)

        self.label_t = QtGui.QLabel(u"Значение T длительности размещения:", self.frame)
        self.gridlay.addWidget(self.label_t, 17, 0)
        self.ln_edit_t = QtGui.QLineEdit(u"", self.frame)
        self.gridlay.addWidget(self.ln_edit_t, 17, 1)

        self.label_inform = QtGui.QLabel(u"* <i>В качестве интервала нужно задать два вещественных числа через пробел<i>", self.frame)
        self.gridlay.addWidget(self.label_inform, 19, 0)

        self.label1 = QtGui.QLabel(u"Корректность ввода:", self.frame)
        self.gridlay.addWidget(self.label1, 21, 0)
        self.label2 = QtGui.QLabel(u"", self.frame)  # Поле для сообщения ошибки
        self.gridlay.addWidget(self.label2, 21, 1)

        self.btn_lay_usecase = QtGui.QHBoxLayout()
        self.button1_usecase = QtGui.QPushButton(u"Задание функций", self.frame)
        self.button2_usecase = QtGui.QPushButton(u"Табулирование интеграла", self.frame)
        self.button3_usecase = QtGui.QPushButton(u"Постановка и решение задачи Коши", self.frame)
        self.btn_lay_usecase.addWidget(self.button1_usecase)
        self.btn_lay_usecase.addWidget(self.button2_usecase)
        self.btn_lay_usecase.addWidget(self.button3_usecase)
        self.gridlay.addLayout(self.btn_lay_usecase, 23, 0, 1, 3)
        self.boxlay.addWidget(self.frame)
        self.connect(self.button1_usecase, QtCore.SIGNAL("clicked()"), self.func_input)
        self.connect(self.button2_usecase, QtCore.SIGNAL("clicked()"), self.int_tab)
        self.connect(self.button3_usecase, QtCore.SIGNAL("clicked()"), self.cauchy)

        self.btn_lay = QtGui.QHBoxLayout()  # Менеджер размещения двух кнопок.
        self.button1 = QtGui.QPushButton(u"Построить графики", self.frame)
        self.button2 = QtGui.QPushButton(u"Отмена ввода/Еще раз", self.frame)
        self.btn_lay.addWidget(self.button1)
        self.btn_lay.addWidget(self.button2)
        self.gridlay.addLayout(self.btn_lay, 25, 0, 1, 2)
        self.boxlay.addWidget(self.frame)
        self.connect(self.button1, QtCore.SIGNAL("clicked()"), self.function)
        self.connect(self.button2, QtCore.SIGNAL("clicked()"), self.clear)

        self.num_segm = 100
        self.num_b = 5
        self.noerr = True

    def tab_func_w(self, func, T, namef, name):
        self.noerr = True
        if tabulate_func_w(func, T, self.num_segm, name) == 0:
            self.label2.setText(u"<font color=red>" + "Некорректный ввод функции " + namef + "!" + "</font>")
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод функции " + namef + "!")
            self.noerr = False

    def tab_func_t(self, func, T, namef, name):
        self.noerr = True
        if tabulate_func_t(func, T, self.num_segm, name) == 0:
            self.label2.setText(u"<font color=red>" + "Некорректный ввод функции " + namef + "!" + "</font>")
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод функции " + namef + "!")
            self.noerr = False


    def func_input(self):
        self.noerr = True
        t = self.ln_edit_t.displayText()
        if not_digit(t):
            self.label2.setText(u"<font color=red>" + "Некорректный ввод значения T!" + "</font>")
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод значения T!")
            self.noerr = False
            return
        self.label2.setText(u"<font color=green>" + "OK" + "</font>")
        T = float(t)
        p = self.ln_edit_p.displayText()
        s = self.ln_edit_s.displayText()
        z = self.ln_edit_z.displayText()
        self.tab_func_w(p, 1, "p(w)", "p(w).csv")
        self.tab_func_t(s, T, "S(t)", "S(t).csv")
        self.tab_func_t(z, T, "z(t)", "z(t).csv")
        if self.noerr:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.information(msg, u"",
                    u"Табулированные функции записаны в соответствующие файлы p(w).csv, S(t).csv, z(t).csv")

    def int_tab(self):
        self.noerr = True
        if tabulate_integral(self.num_segm, "p(w).csv", "U(y).csv") == 0:
            self.label2.setText(u"<font color=red>" + "Не найдена функция p(w)!" + "</font>")
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Не найдена функция p(w)!")
            self.noerr = False
        else:
            self.label2.setText(u"<font color=green>" + "OK" + "</font>")
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.information(msg, u"",
                    u"Табулированный интеграл записан в соответствующий файл U(y).csv")
            func_interp(self.num_segm, "U(y)", 0, 1)

    def pred_cauchy(self, T, b, X, Y):
        self.noerr = True
        for el in [T, X, Y]:
            if not_digit(el):
                self.label2.setText(u"<font color=red>" + "Некорректный ввод численного значения!" + "</font>")
                msg = QtGui.QMessageBox()
                reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод численного значения!")
                self.noerr = False
                return
        try:
            z = pd.read_csv("z(t).csv")
        except Exception:
            self.label2.setText(u"<font color=red>" + "Не введена функция z(t)!" + "</font>")
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Не введена функция z(t)!")
            self.noerr = False
            return
        X = float(X)
        Y = float(Y)
        T = float(T)
        b = float(b)
        diff(z, T, "z(t)", self.num_segm)
        func_interp(self.num_segm, "z(t)_diff", 0, T)
        func_interp(self.num_segm, "S(t)", 0, T)
        func_interp(self.num_segm, "z(t)", 0, T)

    def cauchy(self, T, b, X, Y):
        X = float(X)
        Y = float(Y)
        T = float(T)
        b = float(b)
        try:
            x, y = rugge_kutte_solve('zd(t, z_d) * u(y, U)', self.ln_edit_f.displayText(), X, Y, T/self.num_segm, self.num_segm, b)
        except Exception:
            msg = QtGui.QMessageBox()
            reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Ошибка при решении задачи Коши")
            self.noerr = False
            return

        df = pd.DataFrame()
        df1 = pd.DataFrame()
        t = 0.0
        for i in range(self.num_segm + 1):
            df.loc[i, "point"] = t
            df.loc[i, "value"] = x[i]
            df1.loc[i, "point"] = t
            df1.loc[i, "value"] = y[i]
            t += float(T)/self.num_segm
        df.to_csv("x(t).csv", index=False, encoding='utf-8')
        df1.to_csv("y(t).csv", index=False, encoding='utf-8')
        return

    def function(self):
        self.noerr = True
        self.func_input()
        self.int_tab()
        T = self.ln_edit_t.displayText()
        b = self.ln_edit_b.displayText()
        if self.radio1.isChecked():
            Y = self.ln_edit_y.displayText()
            X = ""
            z = X
            X = self.ln_edit_x.displayText()
            try:
                self.pred_cauchy(T, b, X, Y)
            except Exception:
                self.label2.setText(u"<font color=red>" + "Некорректный ввод b!" + "</font>")
                msg = QtGui.QMessageBox()
                reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод b!")
                self.noerr = False
                return
            self.cauchy(T, b, X, Y)
            if self.noerr:
                T = float(T)
                find_sx(self.num_segm, T)
                find_smx(self.num_segm, T)
                find_c1(self.num_segm, T, 0, b)
                find_c2(self.num_segm, T, 0, b)
                find_F()
                self.graphs_window = PlotGraph()
                self.graphs_window.show()

        else:
            b = b.split()
            if len(b) != 2:
                self.label2.setText(u"<font color=red>" + "Некорректный ввод b!" + "</font>")
                msg = QtGui.QMessageBox()
                reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод b!")
                return
            [b1, b2] = b
            if not_digit(b1) or not_digit(b2) or b2 <= b1:
                self.label2.setText(u"<font color=red>" + "Некорректный ввод b!" + "</font>")
                msg = QtGui.QMessageBox()
                reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"Некорректный ввод b!")
                self.noerr = False
                return
            b1 = float(b1)
            b2 = float(b2)
            b = b1
            Y = '0'
            X = str(pd.read_csv("S(t).csv").loc[0, "value"])
            j = 0
            self.pred_cauchy(T, b, X, Y)

            for b in [b1 + i * (b2 - b1)/(self.num_b - 1) for i in range(self.num_b)]:
                for Y in ['0', '0.2', '0.4', '0.6', '0.8']:
                    self.cauchy(T, b, X, Y)
                    find_c1_by(self.num_segm, float(T), j, b, Y)
                    find_c2_by(self.num_segm, float(T), j, b, Y)
                    j += 1

            find_F_by()
            if self.noerr:
                try:
                    f = pd.read_csv("f(b, y).csv")
                    Fby = []
                    for i in range(f.index.size):
                        Fby += [f.loc[i, 'value']]
                    Y_best = f.loc[np.argmin(np.array(Fby)), 'y']
                    j = 0
                    for b in [b1 + i * (b2 - b1)/(2 * self.num_b - 1) for i in range(2 * self.num_b)]:
                        self.cauchy(T, b, X, Y_best)
                        find_c1(self.num_segm, float(T), j, b)
                        find_c2(self.num_segm, float(T), j, b)
                        j += 1
                    find_F()
                    f = pd.read_csv("f(b).csv")
                    Fb = []
                    for i in range(f.index.size):
                        Fb += [f.loc[i, 'value']]
                    b_best = f.loc[np.argmin(np.array(Fb)), 'point']
                    msg = QtGui.QMessageBox()
                    reply = QtGui.QMessageBox.warning(msg, u"Оптимальные параметры",
                        u"Найдены оптимальные параметры b = " + str(b_best) + ', Y0 = ' + str(Y_best))
                    self.cauchy(T, b_best, str(X), str(Y_best))
                    T = float(T)
                    find_sx(self.num_segm, T)
                    find_smx(self.num_segm, T)
                    self.graphs_window = PlotGraph()
                    self.graphs_window.show()
                except Exception:
                    msg = QtGui.QMessageBox()
                    reply = QtGui.QMessageBox.warning(msg, u"Ошибка", u"ошибка при поиске оптимальных параметров")
                    return

    def clear(self):  # функция отмены ввода, очищающая поля
        self.radio1.setChecked(True)
        self.ln_edit_p.clear()
        self.ln_edit_s.clear()
        self.ln_edit_z.clear()
        self.ln_edit_x.clear()
        self.ln_edit_y.clear()
        self.ln_edit_b.clear()
        self.ln_edit_f.clear()
        self.ln_edit_t.clear()
        self.label2.setText("")

    def closeEvent(self, event):  # функция выхода (нажатие на крестик)
        reply = QtGui.QMessageBox.question(self, u'Выход',
        u"Вы действительно хотите выйти?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


def not_digit(string):
    if string.isdigit():
        return False
    else:
        try:
            float(string)
            return False
        except ValueError:
            return True


def u(y, U):
    if y < 0:
        near = 0
    elif y > 1:
        near = U.index.size - 1
    else:
        near = int(y * (U.index.size - 1))
    return U.loc[near, "spline0"] + U.loc[near, "spline1"]*y +\
             U.loc[near, "spline2"]*y**2 + U.loc[near, "spline3"]*y**3


def zd(t, z_d):
    T = z_d.loc[z_d.index.size - 1, 'point']
    num_segm = z_d.index.size - 1
    near = int(t * num_segm/T)
    return z_d.loc[near, "spline0"] + z_d.loc[near, "spline1"]*t +\
             z_d.loc[near, "spline2"]*t**2 + z_d.loc[near, "spline3"]*t**3


def S_f(t, h, s):
    near = int(t / h)
    return s.loc[near, "spline0"] + s.loc[near, "spline1"]*t +\
             s.loc[near, "spline2"]*t**2 + s.loc[near, "spline3"]*t**3


def z_f(t, h, Z):
    near = int(t / h)
    return Z.loc[near, "spline0"] + Z.loc[near, "spline1"]*t +\
             Z.loc[near, "spline2"]*t**2 + Z.loc[near, "spline3"]*t**3


def rugge_kutte_solve(f, g, x0, y0, h, num_segm, b):  # учитывается вид функций в конкретной задаче
    X = np.zeros(num_segm + 1)
    Y = np.zeros(num_segm + 1)
    X[0] = x0
    Y[0] = y0
    U = pd.read_csv("U(y)_int.csv")
    z_d = pd.read_csv("z(t)_diff_int.csv")
    s = pd.read_csv("S(t)_int.csv")
    Z = pd.read_csv("z(t)_int.csv")
    for i in range(num_segm):
        [t, x, y] = [i * h, X[i], Y[i]]
        [z, S] = [z_f(t, h, Z), S_f(t, h, s)]
        a1 = h * eval(f)
        b1 = h * eval(g)
        [t, x, y] = [i * h + h/2, X[i] + a1/2, Y[i] + b1/2]
        [z, S] = [z_f(t, h, Z), S_f(t, h, s)]
        a2 = h * eval(f)
        b2 = h * eval(g)
        [t, x, y] = [i * h + h/2, X[i] + a2/2, Y[i] + b2/2]
        [z, S] = [z_f(t, h, Z), S_f(t, h, s)]
        a3 = h * eval(f)
        b3 = h * eval(g)
        [t, x, y] = [i * h + h, X[i] + a3, Y[i] + b3]
        [z, S] = [z_f(t, h, Z), S_f(t, h, s)]
        a4 = h * eval(f)
        b4 = h * eval(g)
        X[i + 1] = X[i] + (a1 + 2*a2 + 2*a3 + a4)/6
        Y[i + 1] = Y[i] + (b1 + 2*b2 + 2*b3 + b4)/6
        if Y[i + 1] < 0:
            Y[i + 1] = 0
        if Y[i + 1] > 1:
            Y[i + 1] = 1
    return X, Y


def diff(func, T, name, num_segm):
    df = pd.DataFrame()
    h = T/num_segm
    df.loc[0, "point"] = func.loc[0, "point"]
    df.loc[0, "value"] = (func.loc[1, "value"] - func.loc[0, "value"])/h
    for i in range(1, num_segm):
        df.loc[i, "point"] = func.loc[i, "point"]
        df.loc[i, "value"] = (func.loc[i + 1, "value"] - func.loc[i - 1, "value"])/(2 * h)
    df.loc[num_segm, "point"] = func.loc[num_segm, "point"]
    df.loc[num_segm, "value"] = (func.loc[num_segm, "value"] - func.loc[num_segm - 1, "value"])/h
    df.to_csv(name + "_diff.csv", index=False, encoding='utf-8')
    return


def solve_3diag(R, f):  # решение системы с трехдиагональной матрицей методом прогонки
    num = len(f)

    a = [0]
    b = [R[0][0]]
    c = [R[0][1]]
    for i in range(1, num - 1):
        a += [R[i][i - 1]]
        b += [R[i][i]]
        c += [R[i][i + 1]]
    a += [R[num - 1][num - 2]]
    b += [R[num - 1][num - 1]]
    c += [0]

    p = [0 for _ in range(num + 1)]
    q = [0 for _ in range(num + 1)]
    p[1] = -c[0]/b[0]
    q[1] = f[0]/b[0]
    for k in range(2, num + 1):
        p[k] = -c[k-1]/(a[k-1]*p[k-1]+b[k-1])
        q[k] = (f[k-1] - a[k-1]*q[k-1])/(a[k-1]*p[k-1]+b[k-1])

    res = [0 for _ in range(num + 1)]
    res[num] = q[num]
    for i in range(num-1, -1, -1):
        res[i] = p[i+1]*res[i+1] + q[i+1]
    return res


def func_interp(num_segm, name, start, finish):
    step = (finish - start)/num_segm
    df = pd.read_csv(name + ".csv")
    try:
        m = [0]
        if num_segm >= 2:
            R = [[2/3*step, 1/6*step] + [0 for _ in range(0, num_segm - 3)]]
            for k in range(0, num_segm - 3):
                R += [[0 for l in range(k)] + [1/6*step, 2/3*step, 1/6*step] + [0 for j in range(num_segm - 4 - k)]]
            if num_segm >= 3:
                R += [[0 for _ in range(0, num_segm - 3)] + [1/6*step, 2/3*step]]
            b = [(df.loc[i + 1, "value"] - 2 * df.loc[i, "value"] + df.loc[i - 1, "value"])/step for i in range(1, num_segm)]
            Z = solve_3diag(R, b)
            m += Z + [0]
        A = [(df.loc[i + 1, "value"] - df.loc[i, "value"])/step - (m[i + 1] - m[i])/6*step for i in range(num_segm)]
        B = [df.loc[i, "value"] - m[i]*step*step/6 - A[i]*(start + step*i) for i in range(num_segm)]
        S = [[(m[i + 1] - m[i])/(6*step), (m[i]*(start + step*(i+1)) - m[i + 1]*(start + step*i))/(2*step),
              (m[i + 1]*(start + step*i)**2 - m[i]*(start + step*(i+1))**2)/(2*step) + A[i],
              (m[i]*(start + step*(i+1))**3 - m[i + 1]*(start + step*i)**3)/(6*step) + B[i]] for i in range(num_segm)]
        last = S[len(S) - 1]
        S += [last]
        for i in range(num_segm + 1):
            df.loc[i, "spline0"] = S[i][3]
            df.loc[i, "spline1"] = S[i][2]
            df.loc[i, "spline2"] = S[i][1]
            df.loc[i, "spline3"] = S[i][0]
        df.to_csv(name + "_int.csv", index=False, encoding='utf-8')
        return 1
    except Exception:
        return 0


def find_int(data, start_ind, finish_ind, num_segm):  # находим интеграл по формуле трапеций
    result = 0
    for i in range(start_ind, finish_ind):
        result += (data.loc[i, "value"] + data.loc[i + 1, "value"]) / 2
    return result / num_segm


def tabulate_integral(num_segm, func_name, save_name):
    dfp = pd.read_csv(func_name)
    df = pd.DataFrame()
    try:
        for i in range(num_segm + 1):
            df.loc[i, "point"] = i / num_segm
            df.loc[i, "value"] = find_int(dfp, i, num_segm, num_segm)
        df.to_csv(save_name, index=False, encoding='utf-8')
        return 1
    except Exception:
        return 0


def tabulate_func_w(func, T, num_segm, name):
    df = pd.DataFrame()
    w = 0.0
    try:
        for i in range(num_segm + 1):
            df.loc[i, "point"] = w
            df.loc[i, "value"] = eval(func)
            w += T / num_segm
        if not_digit(str(eval(func))):
            return 0
        else:
            df.to_csv(name, index=False, encoding='utf-8')
            return 1
    except Exception:
        return 0


def tabulate_func_t(func, T, num_segm, name):
    df = pd.DataFrame()
    t = 0.0
    try:
        for i in range(num_segm + 1):
            df.loc[i, "point"] = t
            df.loc[i, "value"] = eval(func)
            t += T / num_segm
        if not_digit(str(eval(func))):
            return 0
        else:
            df.to_csv(name, index=False, encoding='utf-8')
            return 1
    except Exception:
        return 0


def tabulate_func_y(func, T, num_segm, name):
    df = pd.DataFrame()
    y = 0.0
    try:
        for i in range(num_segm + 1):
            df.loc[i, "point"] = y
            df.loc[i, "value"] = eval(func)
            y += T / num_segm
        if not_digit(str(eval(func))):
            return 0
        else:
            df.to_csv(name, index=False, encoding='utf-8')
            return 1
    except Exception:
        return 0


def find_c1(num_segm, T, it, b):
    df = pd.DataFrame()
    w = 0.0
    p = pd.read_csv("p(w).csv")
    for i in range(num_segm + 1):
        df.loc[i, "point"] = w
        df.loc[i, "value"] = w * p.loc[i, "value"]
        w += 1 / num_segm
    y = 0.0
    dfi = pd.DataFrame()
    for i in range(num_segm):
        dfi.loc[i, "point"] = y
        dfi.loc[i, "value"] = find_int(df, i, num_segm, num_segm)
        y += 1/num_segm
    dfi.loc[num_segm, "point"] = 1.0
    dfi.loc[num_segm, "value"] = 0.0
    dfi = dfi.sort_values(by='point', ascending=True)
    dfi.to_csv("U0(y).csv", index=False, encoding='utf-8')
    func_interp(num_segm, "U0(y)", 0, 1)
    dfi = pd.read_csv("U0(y)_int.csv")

    x = pd.read_csv("x(t).csv")
    diff(x, T, 'x(t)', num_segm)
    x_diff = pd.read_csv("x(t)_diff.csv")

    y = pd.read_csv("y(t).csv")
    find_prod(x_diff, dfi, y, num_segm, "xd(t)", "U0(t)", T)
    pr = pd.read_csv("xd(t)U0(t).csv")
    integr = T * find_int(pr, 0, num_segm, num_segm)
    if it > 0:
        c1b = pd.read_csv("c1(b).csv")
    else:
        c1b = pd.DataFrame()
    c1b.loc[it, "point"] = float(b)
    c1b.loc[it, "value"] = 1 - integr/(x.loc[num_segm, "value"] - x.loc[0, "value"])
    c1b.to_csv("c1(b).csv", index=False, encoding='utf-8')
    return


def find_c1_by(num_segm, T, it, b, Y):
    df = pd.DataFrame()
    w = 0.0
    p = pd.read_csv("p(w).csv")
    for i in range(num_segm + 1):
        df.loc[i, "point"] = w
        df.loc[i, "value"] = w * p.loc[i, "value"]
        w += 1 / num_segm
    y = 0.0
    dfi = pd.DataFrame()
    for i in range(num_segm):
        dfi.loc[i, "point"] = y
        dfi.loc[i, "value"] = find_int(df, i, num_segm, num_segm)
        y += 1/num_segm
    dfi.loc[num_segm, "point"] = 1.0
    dfi.loc[num_segm, "value"] = 0.0
    dfi = dfi.sort_values(by='point', ascending=True)
    dfi.to_csv("U0(y).csv", index=False, encoding='utf-8')
    func_interp(num_segm, "U0(y)", 0, 1)
    dfi = pd.read_csv("U0(y)_int.csv")

    x = pd.read_csv("x(t).csv")
    diff(x, T, 'x(t)', num_segm)
    x_diff = pd.read_csv("x(t)_diff.csv")

    y = pd.read_csv("y(t).csv")
    find_prod(x_diff, dfi, y, num_segm, "xd(t)", "U0(t)", T)
    pr = pd.read_csv("xd(t)U0(t).csv")
    integr = T * find_int(pr, 0, num_segm, num_segm)
    if it > 0:
        c1b = pd.read_csv("c1(b, y).csv")
    else:
        c1b = pd.DataFrame()
    c1b.loc[it, "b"] = float(b)
    c1b.loc[it, "y"] = float(Y)
    c1b.loc[it, "value"] = 1 - integr/(x.loc[num_segm, "value"] - x.loc[0, "value"])
    c1b.to_csv("c1(b, y).csv", index=False, encoding='utf-8')
    return


def find_c2(num_segm, T, it, b):
    s = pd.read_csv("s(t).csv")
    x = pd.read_csv("x(t).csv")
    if it > 0:
        c2b = pd.read_csv("c2(b).csv")
    else:
        c2b = pd.DataFrame()
    c2b.loc[it, "point"] = float(b)
    c2b.loc[it, "value"] = abs(x.loc[num_segm, "value"] - s.loc[num_segm, "value"])/s.loc[num_segm, "value"]
    c2b.to_csv("c2(b).csv", index=False, encoding='utf-8')
    return

def find_F():
    c1 = pd.read_csv("c1(b).csv")
    c2 = pd.read_csv("c2(b).csv")
    f = pd.DataFrame()
    for index in c1.index:
        f.loc[index, "point"] = c1.loc[index, "point"]
        f.loc[index, "value"] = c1.loc[index, "value"] * 0.1 + c2.loc[index, "value"]
    f.to_csv("f(b).csv", index=False, encoding='utf-8')
    return


def find_F_by():
    c1 = pd.read_csv("c1(b, y).csv")
    c2 = pd.read_csv("c2(b, y).csv")
    f = pd.DataFrame()
    for index in c1.index:
        f.loc[index, "b"] = c1.loc[index, "b"]
        f.loc[index, "y"] = c1.loc[index, "y"]
        f.loc[index, "value"] = c1.loc[index, "value"] * 0.1 + c2.loc[index, "value"]
    f.to_csv("f(b, y).csv", index=False, encoding='utf-8')
    return


def find_c2_by(num_segm, T, it, b, Y):
    s = pd.read_csv("s(t).csv")
    x = pd.read_csv("x(t).csv")
    if it > 0:
        c2b = pd.read_csv("c2(b, y).csv")
    else:
        c2b = pd.DataFrame()
    c2b.loc[it, "b"] = float(b)
    c2b.loc[it, "y"] = float(Y)
    c2b.loc[it, "value"] = abs(x.loc[num_segm, "value"] - s.loc[num_segm, "value"])/s.loc[num_segm, "value"]
    c2b.to_csv("c2(b, y).csv", index=False, encoding='utf-8')
    return


def find_smx(num_segm, T):
    s = pd.read_csv("S(t).csv")
    x = pd.read_csv("x(t).csv")
    t = 0.0
    df = pd.DataFrame()
    for i in range(num_segm + 1):
        df.loc[i, "point"] = t
        df.loc[i, "value"] = s.loc[i, "value"] - x.loc[i, "value"]
        t += T / num_segm
    df.to_csv("s(t)-x(t).csv", index=False, encoding='utf-8')
    return


def find_sx(num_segm, T):
    s = pd.read_csv("S(t).csv")
    x = pd.read_csv("x(t).csv")
    t = 0.0
    df = pd.DataFrame()
    for i in range(num_segm + 1):
        df.loc[i, "point"] = x.loc[i, "value"]
        df.loc[i, "value"] = s.loc[i, "value"]
        t += T / num_segm
    df.to_csv("s(x).csv", index=False, encoding='utf-8')
    return


def find_prod(z, U, y, num_segm, name1, name2, T):
    df = pd.DataFrame()
    t = 0.0
    for i in range(num_segm + 1):
        near = max(0, int(y.loc[i, "value"] * num_segm))
        near = min(y.index.size - 1, near)
        U_val = U.loc[near, "spline0"] + U.loc[near, "spline1"]*y.loc[i, "value"] +\
             U.loc[near, "spline2"]*y.loc[i, "value"]**2 + U.loc[near, "spline3"]*y.loc[i, "value"]**3
        df.loc[i, "point"] = t
        df.loc[i, "value"] = z.loc[i, "value"] * U_val
        t += T / num_segm
    df.to_csv(name1 + name2 + ".csv")


if __name__=="__main__":
    app = QtGui.QApplication(sys.argv)
    app.setStyle("Cleanlooks")
    app.setStyleSheet(open("./style.qss", "r").read())
    aw = AnyWidget()
    aw.show()
    sys.exit(app.exec_())
